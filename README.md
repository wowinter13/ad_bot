# ad_bot

Test task in the company "Appodeal".

A robot that allows you to create ad slots on site http://target.my.com.

## Mechanism
 
1. Automatically authorizes on the site, using the given email and password

2. Then bot looks for the platform on the transferred `platform_id`

3. Creates new  ad unit.
 

